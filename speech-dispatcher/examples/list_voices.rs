use speech_dispatcher::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let connection = Connection::open("list_voices", "list_voices", "list_voices", Mode::Threaded)?;

    let modules = connection.list_output_modules()?;
    println!("Modules available: {:?}", modules);
    for module in modules {
        if connection.set_output_module(&module).is_ok() {
            println!("Listing voices for module {module}");
        } else {
            println!("Failed to set output module to {module}");
            continue;
        };
        let voices = connection.list_synthesis_voices()?;
        for voice in voices {
            if let Some(variant) = voice.variant {
                println!(
                    "    Name: {} / Language: {} / Variant: {variant}",
                    voice.name, voice.language
                );
            } else {
                println!("    Name: {} / Language: {}", voice.name, voice.language);
            }
        }
    }
    // Use connection.set_synthesis_voice(voice) to set the voice to use.
    Ok(())
}
