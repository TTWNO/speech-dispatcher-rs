use speech_dispatcher::*;
use std::io;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let connection = speech_dispatcher::Connection::open(
        "hello_world",
        "hello_world",
        "hello_world",
        Mode::Threaded,
    )?;
    connection.on_begin(Some(Box::new(|msg_id, client_id| {
        println!("Beginning {} from {}", msg_id, client_id)
    })));
    connection.on_end(Some(Box::new(|msg_id, client_id| {
        println!("Ending {} from {}", msg_id, client_id)
    })));
    let connection_clone = connection.clone();
    drop(connection);
    connection_clone.say(
        Priority::Important,
        format!(
            "Hello, world at rate {} from client {}.",
            connection_clone.get_voice_rate(),
            connection_clone.client_id()
        ),
    );
    connection_clone.set_voice_rate(100)?;
    connection_clone.say(Priority::Important, "This is faster.");
    connection_clone.set_voice_rate(0)?;
    connection_clone.set_spelling(true)?;
    connection_clone.say(Priority::Important, "This is spelled.");
    connection_clone.set_spelling(false)?;
    connection_clone.set_punctuation(Punctuation::All)?;
    connection_clone.say(
        Priority::Important,
        "This statement, unlike others, has punctuation that is spoken!",
    );
    connection_clone.set_punctuation(Punctuation::None)?;
    let mut _input = String::new();
    io::stdin().read_line(&mut _input).unwrap();
    Ok(())
}
